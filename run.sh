#!/bin/bash
cd server
echo "Building docker image..."
docker build -t hugozap/uruit-game .
echo "Running app on port 5555"
docker run -p 5555:8080 -d hugozap/uruit-game
URL='http://localhost:5555'
echo "Trying to launch default browser"
sleep 3

if which xdg-open > /dev/null
then
  xdg-open $URL
elif which gnome-open > /dev/null
then
  gnome-open $URL
else
  open $URL
fi
