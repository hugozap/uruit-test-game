# Solution Notes

[Live Demo](http://209.97.134.190:5555/)

## Running locally (Docker)

A docker image was pushed to the public registry to make it easier
to test the application. The docker contains the node application that serves the react client.

To start the the app on PORT 55555:

```
docker run -p 5555:8080 -d hugozap/uruit-game
```

## Running locally (Manual)

If you don't want to use docker:

### Build client
- Go to the client folder
- run `yarn build`

- Go to the server folder
- run `yarn` to install dependencies
- run `yarn updatefrontend` to copy the frontend build to the server
- run `yarn start` to start the server

## Architecture description

- Redux is used to manage the app state.
- A simple state machine is used to drive the Game UI screens transitions
- React Router is used to handle global routes (Game/Scores/Rule Editor)

### Client Stack

- React
- Redux
- PaperCSS (for the hand draw looks)
- Styled Components
- GunDB (Distributed replication of the scores data)

### Rule Editor

- Changing Rules is supported
- Rule editing state is managed by gameRulesReducer
- Rules are not replicated with server

### Communication with the Backend

- Replication of game scores is supported

A distributed database tool (gundb) is used to manage the
synchronization of game scores.

The replication is automatic. A subscription to the redux 
store is needed to update the gundb and trigger replication

In the backend, the database can use multiple storage options.
For this excercise, a simple test file is used (default option) to store
the database data.

### Testing

Tests are written using the Jest conventions.
Game state and reducers logic are being tested.

## Server architecture

The backend architecture is very simple.

- express framework to handle requests
- the client app is statically served using the static middleware
- the GunDB middleware handles replication network logic.

For testing purposes GunDB uses the file system data provider.
It creates a file called data.json



### TODO:

Some things I didn't manage to finish on time.

- Add flow annotations to all components
- Complete tests (only some components are being tested)
- Add E2E tests (Using something like Cypress)