// @flow
import Gun from "gun";
import type { GameResult } from "../types";
import * as historyActions from "../actions/historyActions";
/**
 * The Gun database instance will hold the games history.
 * We need to update our local store when Gun gets updated
 */
class SharedDatabase {
  gundb: any;
  store: any;

  constructor(store: any) {

    this.gundb = new Gun(window.location.origin + '/gun');
    /**
     * Subscribe to the database updates, this updates
     * may come from the server, so we check that we don't
     * have the records locally. If we have a new record, dispatch
     * the action to store it locally
     */
    this.gundb
      .get("games")
      .map()
      .on(function(game) {
        console.log("gun games added", game);
        //check if we have the game in the global state
        //if we don't, then add it
        const state = store.getState();
        const localGame = state.history.games.find(g => g.id === game.id);
        if (!localGame) {
          store.dispatch(historyActions.recordGame(game));
        }
      });
  }

  /**
   * Add a game record to the Gun database, it will be synchronized
   * with the server automatically.
   * @param {*} game
   */
  addGameRecord(game: GameResult) {
    this.gundb.get("games").set(game);
  }
}

export default SharedDatabase;
