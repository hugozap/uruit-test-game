import { transitions } from "../gameEngine/gameStateMachine";

export function startGame() {
  return {
    type: "UPDATE_GAME_STATE",
    transition: transitions.START
  };
}

export function nextTurn() {
  return {
    type: "UPDATE_GAME_STATE",
    transition: transitions.NEXT_MOVE
  };
}

export function showWinner() {
  return {
    type: "UPDATE_GAME_STATE",
    transition: transitions.FINISH
  };
}

export function playAgain() {
  return {
    type: "UPDATE_GAME_STATE",
    transition: transitions.START
  };
}
