import type { KillRule } from "../types";

// @flow

export function addRule(rule:KillRule) {
    return {
        type: 'ADD_RULE',
        rule
    }
}

export function removeRule(rule:KillRule) {
    return {
        type: 'REMOVE_RULE',
        rule
    }
}