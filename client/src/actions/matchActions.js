// @flow
import {List} from 'immutable';
import type {KillRule} from '../types';

export function startGame() {
    return {
        type: 'START_GAME'
    }
}

export function nextTurn(rules:List<KillRule>, move:string) {
    return {
        type: 'NEXT_TURN',
        move,
        rules
    }
}
/**
 * Clear current match
 */
export function resetMatch() {
    return {
        type: 'RESET_MATCH'
    }
}

export function setPlayer1Name(name) {
    return {
        type: 'SET_PLAYER1_NAME',
        name
    }
}

export function setPlayer2Name(name) {
    return {
        type: 'SET_PLAYER2_NAME',
        name
    }
}