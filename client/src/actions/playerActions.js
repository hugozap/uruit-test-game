export function setPlayer1Name(name) {
    return {
        type: 'SET_PLAYER1_NAME',
        name
    }
}

export function setPlayer2Name(name) {
    return {
        type: 'SET_PLAYER2_NAME',
        name
    }
}