// @flow
import {GameResult} from '../types'
export function recordGame(game: GameResult) {
    return {
        type: 'RECORD_GAME',
        game
    }
}