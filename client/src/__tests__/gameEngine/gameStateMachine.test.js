import {createMachine, transitions} from '../../gameEngine/gameStateMachine'
import * as states from '../../gameEngine/enumMatchStates'

describe('Test transitions', ()=>{
    it('initial state should be CAPTURE_PLAYERS_INFO', () => {
        let m = createMachine();
        expect(m.state).toBe(states.CAPTURE_PLAYERS_INFO)
    })

    it('Emitting the FINISH message while playing sets state to FINISHED', ()=>{
        let m = createMachine(states.WAITING_MOVE);
        m.emit(transitions.FINISH)
        expect(m.state).toBe(states.FINISHED)
    })

    it('Emitting the START message when FINISHED sets state to CAPTURE_PLAYERS_INFO ', ()=>{
        let m = createMachine(states.FINISHED);
        m.emit(transitions.START)
        expect(m.state).toBe(states.CAPTURE_PLAYERS_INFO)
    })

    it('Invalid transitions throw ', ()=>{
        expect(()=>{
            let m = createMachine(states.CAPTURE_PLAYERS_INFO); 
            m.emit(transitions.FINISH)
        }).toThrow()
        
    })
})