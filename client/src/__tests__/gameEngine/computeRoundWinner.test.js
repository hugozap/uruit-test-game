import computeRoundWinner from "../../gameEngine/computeRoundWinner";
import {List} from 'immutable';

const rules = List([{
        killer:'paper',
        killed:'rock'
    },{
        killer:'rock',
        killed: 'scissors'
    }, {
        killer:'scissors',
        killed:'paper'
    }])


describe('computeRoundWinner logic', ()=>{
    it('computes correct winner', ()=>{
        expect(computeRoundWinner(rules, 'paper','rock')).toBe(1)
        expect(computeRoundWinner(rules, 'rock','paper')).toBe(2)
        expect(computeRoundWinner(rules, 'scissors','paper')).toBe(1)
        expect(computeRoundWinner(rules, 'paper','scissors')).toBe(2)
    })

    it('Non registered rule returns -1', () =>{
        expect(computeRoundWinner(rules, 'paper','dog')).toBe(-1)
        
    })
})