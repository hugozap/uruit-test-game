import gameRulesReducer from "../../reducers/gameRulesReducer";
import * as actions from '../../actions/gameRulesActions';
import {List} from 'immutable';

describe('Game Rules Reducer Tests', ()=>{
    it('Add new rule', ()=>{
        const initialState = {
            killRules: List()
        }
        const newState = gameRulesReducer(initialState, actions.addRule({
            killer:'lion',
            killed:'zebra'
        }))
        expect(newState.killRules.find((rule)=>{
            return rule.killer === 'lion' && rule.killed === 'zebra'
        })).not.toBeNull();
    })
})

//TODO: test all