import gameUIReducer from "../../reducers/gameUIReducer";
import * as actions from '../../actions/gameUIActions';
import * as states from "../../gameEngine/enumMatchStates";

import {List} from 'immutable';

describe('Game UI Reducer Tests', ()=>{
    it('Start game transition', ()=>{
        const initialState = {
            gameState: states.CAPTURE_PLAYERS_INFO
        }
        const newState = gameUIReducer(initialState, actions.startGame())
        expect(newState.gameState).toBe(states.WAITING_MOVE);
    })

    it('playing to finish transition', ()=>{
        const initialState = {
            gameState: states.WAITING_MOVE
        }
        const newState = gameUIReducer(initialState, actions.showWinner())
        expect(newState.gameState).toBe(states.FINISHED);
    })

    it('invalid transition should throw', ()=>{
        const initialState = {
            gameState: states.WAITING_MOVE
        }
        expect(()=>{
            //transition from waiting move to start game is not valid.
            const newState = gameUIReducer(initialState, actions.startGame())
        }).toThrow();

    })

})

//TODO: test all