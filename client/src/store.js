import { createStore } from "redux";
import uuid from "uuid";
import rootReducer from "./reducers";
import * as gameUIActions from "./actions/gameUIActions";
import * as matchActions from "./actions/matchActions";
import * as historyActions from "./actions/historyActions";
import * as states from "./gameEngine/enumMatchStates";
import GameDatabase from "./network/GameDatabase";
import type { GameResult } from "./types";

const store = createStore(rootReducer);
const gamesDatabase = new GameDatabase(store);

/**
 * Reactions to store changes:
 * When the current game has a winner, update game state
 * and add the game result to the Gun database
 */
store.subscribe(() => {
  const state = store.getState();
  //If we have a winner update GameUI state and record the match
  if (
    state.gameUI.gameState === states.WAITING_MOVE &&
    state.match.currentMatch.matchWinner
  ) {
    store.dispatch(gameUIActions.showWinner());

    //Dispatch action to record the game in the history record.
    const gameRecord: GameResult = {
      id: uuid.v4(),
      player1: state.match.currentMatch.players.get(0),
      player2: state.match.currentMatch.players.get(1),
      winner: state.match.currentMatch.matchWinner
    };

    gamesDatabase.addGameRecord(gameRecord);
  }
});

export default store;
