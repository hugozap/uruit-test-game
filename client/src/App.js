import React, { Component } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import CapturePlayerInfo from "./containers/CapturePlayerInfo";
import store from "./store";
import CurrentPlayerTurn from "./containers/CurrentPlayerTurn";
import GameContainer from "./containers/GameContainer";
import HistoryRecordsContainer from "./containers/HistoryRecordsContainer";
import { AppWrapper } from "./presentation/common";
import RuleEditorContainer from "./containers/RuleEditorContainer";
import * as ui from "./presentation/common";

const NavBar = () => {
  return (
    <ui.HorizontalList className="border">
      <ui.ListItem>
        <Link to={"/"}>
          <ui.Text className="text-primary"> Play </ui.Text>{" "}
        </Link>
      </ui.ListItem>
      <ui.ListItem>
        <Link className="text-secondary" to={"/scores"}>
          <ui.Text className="text-secondary"> Scores </ui.Text>{" "}
        </Link>
      </ui.ListItem>
      <ui.ListItem>
        <Link className="text-danger" to={"/editor"}>
          <ui.Text className="text-danger"> Rule Editor </ui.Text>{" "}
        </Link>
      </ui.ListItem>
    </ui.HorizontalList>
  );
};

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <AppWrapper>
            <NavBar />
            <Route exact path="/" component={GameContainer} />
            <Route path="/scores" component={HistoryRecordsContainer} />
            <Route path="/editor" component={RuleEditorContainer} />
          </AppWrapper>
        </Router>
      </Provider>
    );
  }
}

export default App;
