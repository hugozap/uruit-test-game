/**
 * Match properties
 */
type Match = {
  rounds: List<RoundResult>,
  moves: List<string>, //List with the current round moves
  players: List<string>,
  matchWinner: ?string
};


type GameResult = {
    id: string,
    player1: string,
    player2: string,
    winner: string
}

/**
 * Defines killer move and killed move
 */
type KillRule = {
  killer: string,
  killed: string
};

export { RoundResult, Match, KillRule, GameResult };
