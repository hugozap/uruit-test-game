import { connect } from 'react-redux'
import ShowMatchWinner from '../presentation/ShowMatchWinner'
import * as gameUIActions from '../actions/gameUIActions'
import * as matchActions from '../actions/matchActions'

const mapStateToProps = (state, ownProps) => {
    return {
        playerName: state.match.currentMatch.matchWinner
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onPlayAgain: () => {
            //Reset match and update game ui state
            dispatch(matchActions.resetMatch())
            dispatch(gameUIActions.playAgain());
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ShowMatchWinner)