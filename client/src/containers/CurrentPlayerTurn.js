// @flow
//TODO: move render component to presentation folder
import React, { Component } from "react";
import { List, Map } from "immutable";
import { connect } from "react-redux";
import * as matchActions from "../actions/matchActions";
import SelectMoveField from "../presentation/SelectMoveField";
import * as ui from "../presentation/common";
import MatchScoreContainer from "./MatchScoreContainer";
import type { KillRule, Match } from "../types";

/**
 * Capture current turn parameters for the current player
 */

type CurrentPlayerTurnProps = {
  currentMatch: Match,
  availableMoves: List<string>,
  onMoveSelected: (List<KillRule>, string) => void,
  killRules: List<KillRule>
};

class CurrentPlayerTurn extends Component<CurrentPlayerTurnProps> {
  itemSelectedHandler(move) {
    this.props.onMoveSelected(this.props.killRules, move);
  }

  render() {
    //If there are no moves, then the current player should be the first one (index 0)
    const currentPlayerIndex = this.props.currentMatch.moves.size;
    const currentPlayerName = this.props.currentMatch.players.get(
      currentPlayerIndex
    );
    const playerClass =
      currentPlayerIndex === 0 ? "text-secondary" : "text-warning";

    return (
      <div>
        <ui.VerticalFlexBox>
          <ui.ListItem>
            <ui.Title>Round {this.props.currentMatch.rounds.size + 1}</ui.Title>
            <ui.SubTitle>
              Player{' '}
              <ui.Text className={playerClass}>{currentPlayerName}</ui.Text>
              {' '}turn:
            </ui.SubTitle>
            <SelectMoveField
              availableMoves={this.props.availableMoves.toArray()}
              onItemSelected={this.itemSelectedHandler.bind(this)}
            />
          </ui.ListItem>
          <ui.ListItem>
            <MatchScoreContainer />
          </ui.ListItem>
        </ui.VerticalFlexBox>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentMatch: state.match.currentMatch,
    availableMoves: getMovesFromRules(state.gameRules.killRules),
    killRules: state.gameRules.killRules
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onMoveSelected: (rules, move) => {
      dispatch(matchActions.nextTurn(rules, move));
    }
  };
};

function getMovesFromRules(killRules: List<KillRule>) {
  let moves = Map();
  killRules.forEach(rule => {
    moves = moves.set(rule.killer, 1).set(rule.killed, 1);
  });
  return List(moves.keys());
}
export default connect(mapStateToProps, mapDispatchToProps)(CurrentPlayerTurn);
