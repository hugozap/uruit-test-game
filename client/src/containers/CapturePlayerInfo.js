// @flow

import React, { Component } from "react";
import { connect } from "react-redux";
import InputNameField from "../presentation/InputNameField";
import * as matchActions from "../actions/matchActions";
import * as gameUIActions from "../actions/gameUIActions";
import * as ui from "../presentation/common";

type CapturePlayerInfoProps = {
  player1Name: string,
  player2Name: string,
  onPlayer1NameChanged: string => void,
  onPlayer2NameChanged: string => void,
  onClickContinue: () => void
};

/**
 * Capture players information
 */
class CapturePlayerInfo extends Component<CapturePlayerInfoProps> {
  player1NameChanged: any => void;
  player2NameChanged: any => void;

  constructor() {
    super();
    this.player1NameChanged = this.player1NameChanged.bind(this);
    this.player2NameChanged = this.player2NameChanged.bind(this);
  }

  player1NameChanged(ev: any) {
    this.props.onPlayer1NameChanged(ev.target.value);
  }

  player2NameChanged(ev: any) {
    this.props.onPlayer2NameChanged(ev.target.value);
  }

  render() {
    const invalidNames =
      this.props.player1Name === this.props.player2Name ||
      this.props.player1Name === "" ||
      this.props.player2Name === "";
    return (
      <div>
        <ui.Title> Players </ui.Title>        
        <InputNameField
          label="Player 1 Name"
          onChange={this.player1NameChanged}
          value={this.props.player1Name}
        />
        <InputNameField
          label="Player 2 Name"
          onChange={this.player2NameChanged}
          value={this.props.player2Name}
        />
        {invalidNames && <div className="alert  alert-secondary"> Please enter the player names</div>}
        {/* TODO: disable if invalid names */}
        <ui.Button className="btn-primary" disabled={invalidNames} onClick={this.props.onClickContinue}>
          {" "}
          Start{" "}
        </ui.Button>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    player1Name: state.match.currentMatch.players.get(0),
    player2Name: state.match.currentMatch.players.get(1)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPlayer1NameChanged: name => {
      dispatch(matchActions.setPlayer1Name(name));
    },
    onPlayer2NameChanged: name => {
      dispatch(matchActions.setPlayer2Name(name));
    },
    onClickContinue: () => {
      dispatch(gameUIActions.startGame());
      dispatch(matchActions.startGame());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CapturePlayerInfo);
