import { connect } from "react-redux";
import RuleEditor from "../presentation/RuleEditor";
import * as gameRulesActions from "../actions/gameRulesActions";

const mapStateToProps = (state, ownProps) => {
  return {
    rules: state.gameRules.killRules
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onAddRule: rule => {
      dispatch(gameRulesActions.addRule(rule));
    },
    onRemoveRule: rule => {
      dispatch(gameRulesActions.removeRule(rule));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RuleEditor);
