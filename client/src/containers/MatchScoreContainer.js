import { connect } from 'react-redux'
import MatchScore from '../presentation/MatchScore'
import matchActions from '../actions/matchActions' 

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.match.currentMatch.rounds
    }
}



export default connect(mapStateToProps)(MatchScore)