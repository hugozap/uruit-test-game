import { connect } from 'react-redux'
import HistoryRecordsContainer from '../presentation/HistoryRecords'

const mapStateToProps = (state, ownProps) => {
    return {
        games: state.history.games
    }
}


export default connect(mapStateToProps)(HistoryRecordsContainer)