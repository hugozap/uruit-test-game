import { connect } from "react-redux";
import React, { Component } from "react";
import * as states from "../gameEngine/enumMatchStates";
import CurrentPlayerTurn from "./CurrentPlayerTurn";
import CapturePlayerInfo from "./CapturePlayerInfo";
import ShowMatchWinnerContainer from "./ShowMatchWinnerContainer";
/**
 * Game wrapper component
 * TODO: add flow annotations
 */
class GameContainer extends Component {
  render() {
    switch (this.props.gameState) {
      case states.CAPTURE_PLAYERS_INFO:
        return <CapturePlayerInfo />;
      case states.WAITING_MOVE:
        return <CurrentPlayerTurn />;
      case states.FINISHED:
        return <ShowMatchWinnerContainer/>;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    gameState: state.gameUI.gameState
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(GameContainer);
