// @flow
import React, { Component } from "react";
import { List } from "immutable";
import styled from "styled-components";
import * as ui from "./common";
import type { KillRule } from "../types";

type RuleEditorProps = {
  rules: List<KillRule>,
  onAddRule: (rule: KillRule) => void,
  onRemoveRule: (rule: KillRule) => void
};

type RuleEditorState = {
  newRule: KillRule
};

const NewRuleStyledContainer = styled(ui.HorizontalFlexBox)`
  padding: 1rem;
`;

class RuleEditor extends Component<RuleEditorProps, RuleEditorState> {
  constructor() {
    super();
    this.state = {
      newRule: {
        killer: "",
        killed: ""
      }
    };
  }

  newRuleKillerChanged(ev) {
    this.setState({
      ...this.state,
      newRule: { ...this.state.newRule, killer: ev.target.value }
    });
  }

  newRuleKilledChanged(ev) {
    this.setState({
      ...this.state,
      newRule: { ...this.state.newRule, killed: ev.target.value }
    });
  }

  addRuleHandler(ev) {
    this.props.onAddRule(this.state.newRule);
    this.setState({...this.state, newRule: {
        killer:'',
        killed:''
    }})
    ev.preventDefault();
  }

  removeRuleHandler(selectedRule: KillRule) {
    this.props.onRemoveRule(selectedRule);
  }

  render() {
    return (
      <div>
        <ui.Title> Game Rules Editor </ui.Title>
        <ui.SubTitle> Add New Rule </ui.SubTitle>
        <form onSubmit={this.addRuleHandler.bind(this)}>
          <NewRuleStyledContainer className="border">
            <ui.Input
              value={this.state.newRule.killer}
              onChange={this.newRuleKillerChanged.bind(this)}
              placeholder="Killer"
            />
            <ui.Input
              value={this.state.newRule.killed}
              onChange={this.newRuleKilledChanged.bind(this)}
              placeholder="Killed"
            />
            <ui.Button> Add </ui.Button>
          </NewRuleStyledContainer>
        </form>

        <ui.SubTitle> Rules </ui.SubTitle>
        <ui.VerticalList>
          {this.props.rules.map((rule: KillRule, ix) => {
            return (
              <ui.ListItem key={ix}>
                <ui.HorizontalFlexBox>
                  <ui.Text className="badge success">{rule.killer}</ui.Text>
                  <ui.Text>kills</ui.Text>
                  <ui.Text className="badge warning">{rule.killed}</ui.Text>
                  <button
                    className="btn-danger"
                    onClick={this.removeRuleHandler.bind(this, rule)}
                  >
                    {" "}
                    Delete{" "}
                  </button>
                </ui.HorizontalFlexBox>
              </ui.ListItem>
            );
          })}
        </ui.VerticalList>
      </div>
    );
  }
}

export default RuleEditor;
