// @flow
import React from "react";
import { Button, HorizontalList, ListItem } from "./common";
import styled from "styled-components";

const StyledListItem = styled(ListItem)`
  display: inline-block;
`;

type SelectMoveFieldProps = {
  availableMoves: Array<string>,
  onItemSelected: string => void
};
/**
 * Next move selection for current round
 */
const SelectMoveField = (props: SelectMoveFieldProps) => {
  return (
    <ul className="border-dashed">
      {props.availableMoves.map(item => {
        return (
          <StyledListItem key={item} onClick={props.onItemSelected.bind(null, item)}>
            <Button className="btn-warning"> {item} </Button>
          </StyledListItem>
        );
      })}
    </ul>
  );
};

export default SelectMoveField;
