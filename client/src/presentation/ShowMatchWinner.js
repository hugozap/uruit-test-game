import React, { Component } from 'react';
import * as ui from './common'
import styled from 'styled-components';

const StyledContainer = styled(ui.Box)`
    padding: 1rem;
`



//TODO: flow annotations
class ShowMatchWinner extends Component {
    render() {
        return (
            <StyledContainer>
                <ui.Title>We Have A Winner </ui.Title>
                <ui.SubTitle> {this.props.playerName} is the new EMPEROR!  </ui.SubTitle>
                <ui.Button className="btn-secondary" onClick={this.props.onPlayAgain}> Play Again </ui.Button>
            </StyledContainer>
        );
    }
}

export default ShowMatchWinner;