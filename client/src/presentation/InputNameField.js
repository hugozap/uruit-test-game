// @flow
import React from "react";
import styled from 'styled-components';

const FieldContainer = styled.div`
  padding-bottom: 1rem;
`

type InputNameFieldProps = {
  label: string,
  onChange: (ev: any) => void,
  value: string
};

const InputNameField = (props: InputNameFieldProps) => {
  return (
    <FieldContainer>
      <label>{props.label || "Name"}</label>
      <input value={props.value} onChange={props.onChange} />
    </FieldContainer>
  );
};

export default InputNameField;
