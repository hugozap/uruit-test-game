/**
 * Common UI components
 */
import React from "react";
import "../../node_modules/papercss/dist/paper.css";
import styled from "styled-components";

const LargeButton = props => {
  return <button className="btn-large" {...props} />;
};

const Button = props => {
  return <button {...props} />;
};

const HorizontalList = styled.ul`
  display: flex;
  flex-direction: row;
  padding: 0;
  margin: 0;
  list-style-type: none;
`;

const VerticalList = styled.ul`
  display: flex;
  flex-direction: column;
  padding: 0;
  margin: 0;
  list-style-type: none;
`;
const ListItem = styled.li`
  list-style-type: none;
  text-indent: 0;
  &:before {
    content:""
  }
  padding: 0.5rem;
`;

const Title = props => {
  return <h2 className="card-title" {...props} />
};

const SubTitle = props => {
  return <h4 className="card-subtitle" {...props} />
};

const Text = styled.span `
`
const BoldText = styled(Text) `
  font-weight: 700;
`
const Box = props => {
  return <div className="card" {...props} />
}
const PaddedBox = styled(Box)`
  padding:2rem;
`
const AppWrapper = styled.div`
  width: 30rem;
  padding-top:5rem;
  margin: auto;
  display: flex;
  align-items: center;
  flex-direction: column;

`

const HorizontalFlexBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const VerticalFlexBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`


const Input = styled.input`

`

export { AppWrapper, Box, PaddedBox, Button, LargeButton, HorizontalFlexBox, VerticalFlexBox, HorizontalList, VerticalList, ListItem, Title, SubTitle , Text, BoldText, Input};
