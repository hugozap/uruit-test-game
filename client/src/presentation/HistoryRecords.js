// @flow
import React, { Component } from "react";
import type { GameResult } from "../types";
import { List } from "immutable";
import * as ui from "./common";
import styled from "styled-components";

const PlayerNameStyledText = styled(ui.Text)`
  width: 33%;
  display: inline-block;
`;

const PlayerNameStyledTextHeader = styled(ui.BoldText)`
  width: 33%;
  display: inline-block;
`;

type HistoryRecordsProps = {
  games: List<GameResult>
};

type GameResultItemProps = {
  game: GameResult
};

const GameResultItem = (props: GameResultItemProps) => {
  return (
    <ui.ListItem className="" >
      <PlayerNameStyledText> {props.game.player1} </PlayerNameStyledText>
      <PlayerNameStyledText> {props.game.player2} </PlayerNameStyledText>
      <PlayerNameStyledText className="text-success"> {props.game.winner} </PlayerNameStyledText>
    </ui.ListItem>
  );
};

class HistoryRecords extends Component<HistoryRecordsProps> {
  render() {
    return (
      <ui.PaddedBox>
        <ui.Title> Previous Games Results </ui.Title>
        <ui.VerticalList className="child-borders">
          <ui.ListItem className="background-primary">
            <PlayerNameStyledTextHeader> Player 1 </PlayerNameStyledTextHeader>
            <PlayerNameStyledTextHeader> Player 2</PlayerNameStyledTextHeader>
            <PlayerNameStyledTextHeader>
              Match Winner
            </PlayerNameStyledTextHeader>
          </ui.ListItem>

          {this.props.games.map((game: GameResult) => {
            return <GameResultItem key={game.id} game={game} />;
          })}
        </ui.VerticalList>
      </ui.PaddedBox>
    );
  }
}

export default HistoryRecords;
