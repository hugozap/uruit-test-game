import React, { Component } from "react";
import {
  Title,
  SubTitle,
  VerticalList,
  ListItem,
  Text,
  HorizontalList,
  HorizontalFlexBox
} from "./common";
import styled from "styled-components";

/**
 * List scores for the current match
 * TODO: add flow annotations
 */

const StyledRoundWinnerText = styled(Text)`
  font-weight: 700;
`;

const RoundContainer = styled(HorizontalFlexBox)`
  width:10rem;
`;
class MatchScore extends Component {
  render() {
    return (
      <div>
        <SubTitle>Round Scores</SubTitle>
        <VerticalList className="child-borders">
          {this.props.items.map((item, ix) => {
            return (
              <ListItem key={ix}>
                <RoundContainer>
                  <Text> Round {ix + 1} </Text>
                  <StyledRoundWinnerText className="text-success">
                    {item || "no winner"}
                  </StyledRoundWinnerText>
                </RoundContainer>
              </ListItem>
            );
          })}
        </VerticalList>
      </div>
    );
  }
}

export default MatchScore;
