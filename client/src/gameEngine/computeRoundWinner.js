// @flow
import {List} from 'immutable'
import type {KillRule} from '../types'

/**
 * Returns 1 (player 1 move wins), 2 (player 2 move wins) or -1 (no winner)
 * according to the list of rules
 */
export default function computeRoundWinner(rules:List<KillRule>, move1:string, move2:string) {
    const winnerMove = rules.find((rule) => {
        return (rule.killer === move1 && rule.killed === move2) ||
        (rule.killer === move2 && rule.killed === move1)
    })
    if(winnerMove) {
        return winnerMove.killer === move1 ? 1 : 2
    }
    return -1;
}