import {combineReducers} from 'redux';
import matchReducer from './matchReducer';
import gameUIReducer from './gameUIReducer';
import gameRulesReducer from './gameRulesReducer';
import historyReducer from './historyReducer';

const rootReducer = combineReducers({
    match: matchReducer,
    gameUI: gameUIReducer,
    gameRules: gameRulesReducer,
    history: historyReducer
})

export default rootReducer