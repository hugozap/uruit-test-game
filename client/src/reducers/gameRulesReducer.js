// @flow

import { List } from "immutable";
import type { KillRule } from "../types";

/**
 * State related to the game rules
 */

type RulesState = {
  killRules: List<KillRule>
};

const initialState = {
  killRules: List([
    {
      killer: "paper",
      killed: "rock"
    },
    {
      killer: "rock",
      killed: "scissors"
    },
    {
      killer: "scissors",
      killed: "paper"
    }
  ])
};

export default function(state: RulesState = initialState, action: any) {
  switch (action.type) {
    case "ADD_RULE":
      const isRuleValid = validateRule(state.killRules, action.rule);
      if (isRuleValid) {
        return { ...state, killRules: state.killRules.push(action.rule) };
      } else {
        console.log("invalid rule");
        return state;
      }
    case "REMOVE_RULE":
      //Remove the rule that corresponds the action.killer and action.killed values
      return {
        ...state,
        killRules: state.killRules.filter(
          rule =>
            !(
              rule.killer === action.rule.killer &&
              rule.killed === action.rule.killed
            )
        )
      };
    default:
      return state;
  }
}

/**
 *
 * validates that the rule doesn't conflict with others
 * @param {} rules
 * @param {*} rule
 */
function validateRule(rules: List<KillRule>, rule: KillRule) {
  if(rule.killer.trim() === '' || rule.killed.trim() === '') {
      return false;
  }
  const opposite = rules.find(r => {
    return r.killer === rule.killed && r.killed === rule.killer;
  });
  const sameRule = rules.find(r => {
    return r.killer === rule.killer && r.killed === rule.killed;
  });
  //if we found a rule that's the opposite, or one that's exactly the same then the rule is not compatible
  return opposite == null && sameRule == null;
}
