// @flow
import { List, Map } from "immutable";
import computeRoundWinner from "../gameEngine/computeRoundWinner";
import type { KillRule, Match } from "../types";

/**
 * Match state and logic
 */

type MatchState = {
  currentMatch: Match
};

const initialState: MatchState = {
  currentMatch: newMatch()
};

/**
 * Initializes a new match
 */
function newMatch():Match {
  return {
    rounds: List(),
    moves: List(),
    players: List(["", ""]),
    matchWinner: null
  };
}

export default function matchReducer(
  state: MatchState = initialState,
  action: any
) {
  switch (action.type) {
    case "SET_PLAYER1_NAME":
      return {
        ...state,
        currentMatch: {
          ...state.currentMatch,
          players: state.currentMatch.players.set(0, action.name)
        }
      };
    case "SET_PLAYER2_NAME":
      return {
        ...state,
        currentMatch: {
          ...state.currentMatch,
          players: state.currentMatch.players.set(1, action.name)
        }
      };

    case "START_GAME":
      return {
        ...state,
        currentMatch: {
          ...state.currentMatch,
          moves: List()
        }
      };
    case "NEXT_TURN":
      /* action.move contains the current move
         if the moves array already has 1 element
         we have to compute the round result */
      return computeStateAfterTurn(state, action.move, action.rules);

    case "ADD_ROUND_RESULT":
      return {
        ...state,
        currentMatch: {
          ...state.currentMatch,
          rounds: state.currentMatch.rounds.push(action.round)
        }
      };
    case "RESET_MATCH":
      return {
        ...state,
        currentMatch: newMatch()
      };

    default:
      return state;
  }
}

/**
 * After the last move, computes the new state, check if there's
 * a winner
 * @param {*} state
 * @param {*} lastMove
 */
function computeStateAfterTurn(state, lastMove: string, rules: List<KillRule>) {
  if (state.currentMatch.moves.size === 1) {
    const firstMove = state.currentMatch.moves.get(0);
    //compute winner and archive round
    const winnerNumber = computeRoundWinner(rules, firstMove, lastMove);
    const winnerName =
      winnerNumber === -1
        ? null
        : state.currentMatch.players.get(winnerNumber - 1);
    const rounds = state.currentMatch.rounds.push(winnerName);
    //Do we have a winner?
    //Lets generate a map with the number of wins per player
    //example: <'john':4, 'carlos':2>
    const resultMap = rounds.reduce((prev, current) => {
      if (!current) {
        //when nobody wins current will be null
        return prev;
      }
      return prev.set(current, (prev.get(current) || 0) + 1);
    }, new Map());

    const winner = resultMap.findKey(matchesWon => {
      return matchesWon === 3;
    });

    //Reset moves, update rounds
    return {
      ...state,
      currentMatch: {
        ...state.currentMatch,
        rounds,
        moves: List(),
        matchWinner: winner
      }
    };
  }

  //Otherwise just update the moves
  return {
    ...state,
    currentMatch: {
      ...state.currentMatch,
      moves: state.currentMatch.moves.push(lastMove)
    }
  };
}
