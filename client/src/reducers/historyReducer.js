// @flow
import { List } from "immutable";
import {GameResult} from '../types';

/**
 * Store and retrieve games(matches) history.
 */

type HistoryState = {
    games: List<GameResult>
}

const initialState:HistoryState = {
  games: List()
};

export default function(state:HistoryState = initialState, action: any) {
  switch (action.type) {
    case "RECORD_GAME":
      return { ...state, games: state.games.push(action.game) };
    default:
      return state;
  }
}
