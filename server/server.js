const express = require("express");
const Gun = require("gun");
const path = require("path");

const PORT = process.env.PORT || 8080;
const HOST = "0.0.0.0";

// App
const app = express();
app.use(Gun.serve);
//Serve react app for the 3 paths
app.use(express.static(path.join(__dirname,"public")));
app.get(['/','/editor','/scores'], function (req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

const server = app.listen(PORT, HOST);
//Create Gun instance (using file datastore for now)
var gun = Gun({
  file: "data.json",
  web: server
});

console.log(`Running  on http://${HOST}:${PORT}`);
